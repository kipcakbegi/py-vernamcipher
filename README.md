**Vernam şifrelemesini uygulayabileceğiniz basit bir Python betiği.**

## Kullanımı ##

**Şifreleme**

Aynı uzunluktaki metini, **belirli bir anahtar** ile şifrelemek için:

```
#!python

python2 py-vernamcipher.py -e [sifrelenecek_metin] [anahtar]
```


Aynı uzunluktaki metini, **rastgele bir anahtar** ile şifrelemek için:

```
#!python

python2 py-vernamcipher.py -e [sifrelenecek_metin] -rkey
```


**Şifre çözme**:

```
#!python

python2 py-vernamcipher.py -d [sifrelenmis_metin] [anahtar]
```